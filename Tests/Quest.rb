module GamePlay
  # Menu to choose which list of quest to show
  class QuestBookMenu < Base
    include UI #Add the UI functions to the current interface
    include Text::Util #Add the text functions to the interface
    # Create a new QuestBookMenu
    def initialize
      super() # Call the Base initialize method ignoring the argument of the current initialize
      @viewport = Viewport.create(:main, 1000) # Viewport that holds the sprites of the interface
      init_text(0, @viewport) # Initialize the add_text function
      @is_showing_failed = isf = $quests.failed_quests.size > 0 # Testing if we show failed quest or not
      # Showing the background
      Sprite.new(@viewport).set_bitmap(isf ? "quest/quest_bg_1" : "quest/quest_bg_1_2", :interface)
      # Showing the texts
      add_text(0, isf ? 39 : 53, 320, 23, "Quêtes principales", 1, 1).load_color(9)
      add_text(0, isf ? 84 : 106, 320, 23, "Quêtes secondaires", 1, 1).load_color(9)
      add_text(0, 129, 320, 23, "Quêtes échouées", 1, 1).load_color(9) if isf # If set after "quitter", there will be error with index
      add_text(0, isf ? 174 : 159, 320, 23, "Quitter", 1, 1).load_color(9)
      # Showing the selector
      @index = 0
      @selector = Sprite.new(@viewport)
        .set_position(0, @texts[@index].y + FOY) # FOY is a factor of ajustment stored in Text::Util
        .set_bitmap("quest/quest_selector", :interface)
    end
    
    # Updates the interface
    def update
      return unless super # The update method from base tells if the update can continue or not (message display)
      max_index = @is_showing_failed ? 3 : 2
      if index_changed(:@index, :UP, :DOWN, max_index) # Check the user input affecting the index
        @selector.set_position(0, @texts[@index].y + FOY) # Adjust the selector position
      end
      if Input.trigger?(:A)
        return @running = false if @index == max_index
        # Start the quest list scene according to the index
        call_scene(QuestBookList, [:primary, :secondary, :failed][@index])
      end
      @running = false if Input.trigger?(:B)
    end
    
    # Dispose the viewports of the interface
    def dispose
      super # Call the dispose from Base to dispose the messagebox
      @viewport.dispose #Under LiteRGSS Viewport#dispose disposes all the sprite it holds
    end
  end
end

def add_quests
  $game_data_quest = $game_data_quest * 10
  20.times do |i| $quests.start(i) end
  5.times do |i| $game_data_quest[i].primary = true end
end

module GamePlay
  # Menu to choose which quest to show
  class QuestBookList < Base
    include Text::Util #Add the text functions to the interface
    # Create a new QuestBookList
    # @param type [Symbol] type of the list to show
    def initialize(type)
      super() # Call the Base initialize method ignoring the argument of the current initialize
      @viewport = Viewport.create(:main, 1000) # Viewport that holds the sprites of the interface
      @viewport2 = Viewport.create(80, 24, 160, 189, 1001)
      init_text(0, @viewport2) # Initialize the add_text function
      # Showing the background
      Sprite.new(@viewport).set_bitmap("quest/quest_bg_list", :interface)
      __get_quests(type)
      @type = type
      # Showing the texts
      @quests_name.each_with_index do |name, i|
        add_text(1, i * 23, 160, 23, name).load_color($quests.finished?(@quests_id[i]) ? 11 : 9)
      end
      add_text(0, @quests_name.size * 23, 160, 23, "Retour", 1).load_color(9)
      # Showing the selector
      @index = 0
      @selector = Sprite.new(@viewport2)
        .set_position(0, 0)
        .set_bitmap("quest/quest_selector_list", :interface)
    end
    
    # Updates the interface
    def update
      return unless super # The update method from base tells if the update can continue or not (message display)
      max_index = @quests_name.size
      if index_changed(:@index, :UP, :DOWN, max_index) # Check the user input affecting the index
        @selector.set_position(0, @index * 23) # Adjust the selector position
        adjust_viewport
      end
      if Input.trigger?(:A)
        return @running = false if @index == max_index
        call_scene(QuestBookQuest, @quests_id[@index], @index, @quests_id)
      end
      @running = false if Input.trigger?(:B)
    end
    
    # Adjust the viewport position
    def adjust_viewport
      return if @quests_name.size < 8
      if @index >= 4 and @index < @quests_name.size - 4
        @viewport2.oy = (@index - 3) * 23
      elsif @index < 4
        @viewport2.oy = 0
      else
        @viewport2.oy = (@quests_name.size - 7) * 23
      end
    end
    
    # Get the quests to list
    # @param type [Symbol] type of the list to show
    def __get_quests(type)
      case type
      when :primary
        @quests_id = ($quests.active_quests.keys + $quests.finished_quests.keys).select { |id| GameData::Quest.is_primary?(id) }
      when :secondary
        @quests_id = ($quests.active_quests.keys + $quests.finished_quests.keys).select { |id| !GameData::Quest.is_primary?(id) }
      when :failed
        @quests_id = $quests.failed_quests.keys
      end
      @quests_name = @quests_id.collect { |id| _get(45, id) }
    end
    
    # Change the viewport visibility of the scene
    # @param v [Boolean]
    def visible=(v)
      @viewport2.visible = v
      super
    end
    
    # Dispose the viewports of the interface
    def dispose
      super # Call the dispose from Base to dispose the messagebox
      @viewport.dispose #Under LiteRGSS Viewport#dispose disposes all the sprite it holds
      @viewport2.dispose
    end
  end
end


module GamePlay
  # Show the quest info
  class QuestBookQuest < Base
    include Text::Util #Add the text functions to the interface
    # Create a new QuestBookList
    # @param id [Integer] Id of the quest to show
    # @param index [Integer] Index of last scene
    # @param quests_id [Array<Integer>] List of quest ids
    def initialize(id, index, quests_id)
      super() # Call the Base initialize method ignoring the argument of the current initialize
      @index = index
      @id = id
      @quests_id = quests_id
      @viewport = Viewport.create(:main, 1000) # Viewport that holds the sprites of the interface
      @viewport2 = Viewport.create(4, 115, 312, 64, 1001)
      init_text(0, @viewport) # Initialize the add_text function
      # Showing the background
      Sprite.new(@viewport).set_bitmap("quest/quest_bg_2", :interface)
      # Showing the selector
      @selector = Sprite.new(@viewport2)
        .set_position(0, 0)
        .set_bitmap("quest/quest_selector", :interface)
      @objective_index = 0
      @selector.src_rect.height = 16
      @selector.z = 1
      # Showing the texts
      @quest_name = add_text(5, 24, 310, 23, nil.to_s, 1)
      @quest_descr = add_text(5, 49, 310, 16, nil.to_s).load_color(9)
      @quest_earnings = Array.new(4) do |i|
        add_text(5 + 156 * (i % 2), 181 + 16 * (i / 2), 154, 16, nil.to_s)
      end
      @objective_stack = UI::SpriteStack.new(@viewport2)
      show_quest
    end
    
    # Updates the interface
    def update
      return unless super # The update method from base tells if the update can continue or not (message display)
      max_index = @quests_id.size - 1
      if index_changed(:@objective_index, :UP, :DOWN, @objective_max) # Check the user input affecting the index
        @selector.set_position(0, @objective_index * 16) # Adjust the selector position
        adjust_viewport
      end
      if index_changed(:@index, :LEFT, :RIGHT, max_index) # Check the user input affecting the index
        @id = @quests_id[@index]
        show_quest
      end
      @running = false if Input.trigger?(:B)
    end
    
    # Show the quest info
    def show_quest
      quest = GameData::Quest.quest(@id)
      return unless quest
      @quest_name.text = _get(45, @id)
      @quest_name.load_color($quests.finished?(@id) ? 11 : ($quests.failed?(@id) ? 12 : 9))
      @quest_descr.multiline_text = _get(46, @id)
      @objective_stack.dispose
      show_objective(quest)
      color = $quests.earnings_got?(@id) ? 12 : 18
      @quest_earnings.each_with_index do |text, i|
        if earning = quest.earnings[i]
          show_earning(text, earning)
          text.load_color(color)
        else
          text.visible = false
        end
      end
    end
    
    # Show an earning
    # @param text [LiteRGSS::Text] a text object
    # @param earning [Hash] earning data
    def show_earning(text, earning)
      if earning[:money]
        text.text = "#{earning[:money]}$"
      elsif earning[:item]
        text.text = "#{earning[:item_amount]} #{_get(12, earning[:item])}"
      end
      text.visible = true
    end
    
    # Show the objective of a quest
    # @param quest_data [GameData::Quest]
    def show_objective(quest_data)
      order = quest_data.goal_order
      stack = @objective_stack
      stack.dispose
      index = 0
      quest = $quests.active_quests.fetch(@id, nil)
      quest = $quests.finished_quests.fetch(@id, nil) unless quest
      quest = $quests.failed_quests.fetch(@id, nil) unless quest
      order.each_with_index do |type, i|
        next unless $quests.goal_shown?(@id, i)
        y = index * 16
        index_in_table = $quests.get_goal_data_index(@id, i)
        case type
        when :items
          undone = (nb = quest[:items][index_in_table]) < (amount = quest_data.item_amount[index_in_table])
          stack.add_text(1, y, 310, 16, 
            "Trouver #{amount} #{_get(12, quest_data.items[index_in_table])} (#{nb})")
            .load_color(undone ? 18 : 11)
        when :speak_to
          stack.add_text(1, y, 310, 16, 
            "Parler à #{quest_data.speak_to[index_in_table]}")
            .load_color(quest[:spoken][index_in_table] ? 11 : 18)
        when :see_pokemon
          stack.add_text(1, y, 310, 16, 
            "Voir un #{_get(0, quest_data.see_pokemon[index_in_table])}")
            .load_color(quest[:pokemon_seen][index_in_table] ? 11 : 18)
        when :beat_pokemon
          undone = (nb = quest[:pokemon_beaten][index_in_table]) < (amount = quest_data.beat_pokemon_amount[index_in_table])
          stack.add_text(1, y, 310, 16, 
            "Battre #{amount} #{_get(0, quest_data.beat_pokemon[index_in_table])} (#{nb})")
            .load_color(undone ? 18 : 11)
        when :catch_pokemon
          undone = (nb = quest[:pokemon_catch][index_in_table]) < (amount = quest_data.catch_pokemon_amount[index_in_table])
          stack.add_text(1, y, 310, 16, 
            "Capturer #{amount} #{_convert_catch(quest_data.catch_pokemon[index_in_table])} (#{nb})")
            .load_color(undone ? 18 : 11)
        when :beat_npc
          undone = (nb = quest[:npc_beaten][index_in_table]) < (amount = quest_data.beat_npc_amount[index_in_table])
          stack.add_text(1, y, 310, 16, 
            "Battre #{quest_data.beat_npc[index_in_table]} #{amount} fois (#{nb})")
            .load_color(undone ? 18 : 11)
        when :get_egg_amount
          undone = (nb = quest[:egg_counter]) < (amount = quest_data.get_egg_amount)
          stack.add_text(1, y, 310, 16, 
            "Trouver #{amount} œufs (#{nb})")
            .load_color(undone ? 18 : 11)
        when :hatch_egg_amount
          undone = (nb = quest[:egg_hatched]) < (amount = quest_data.hatch_egg_amount)
          stack.add_text(1, y, 310, 16, 
            "Faire éclore #{amount} œufs (#{nb})")
            .load_color(undone ? 18 : 11)
        else
          next
        end
        index += 1
      end
      @objective_index = 0
      @objective_max = index - 1
      @selector.set_position(0, 0)
      @viewport2.oy = 0
      @viewport2.sort_z
    end
    # Fonction de conversion en message de l'objectif de capture
    # @param data [Integer, Hash]
    # @return [String]
    def _convert_catch(data)
      if data.is_a?(Integer)
        return _get(0, data)
      end
      str = "Pokémon"
      if id = data[:type]
        str << " de type #{$game_data_types[id].name}"
      end
      if id = data[:nature]
        str << " ayant la nature #{_get(8, id)}"
      end
      if id = data[:min_level]
        str << " de niveau #{id} minimum"
        if id = data[:max_level]
          str << " et de niveau #{id} maximum"
        end
      elsif id = data[:max_level]
        str << " de niveau #{id} maximum"
      end
      if id = data[:level]
        str << " au niveau #{id}"
      end
      return str
    end
    # Adjust the viewport position
    def adjust_viewport
      return if @objective_max < 5
      if @objective_index >= 3
        @viewport2.oy = (@objective_index - 3) * 16
      elsif @objective_index < 4
        @viewport2.oy = 0
      end
    end
    # Dispose the viewports of the interface
    def dispose
      super # Call the dispose from Base to dispose the messagebox
      @viewport.dispose #Under LiteRGSS Viewport#dispose disposes all the sprite it holds
      @viewport2.dispose
    end
  end
end
# Tell the PSDK Test what to test
Tester.start(GamePlay::QuestBookMenu)