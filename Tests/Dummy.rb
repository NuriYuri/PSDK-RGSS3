class Dummy < GamePlay::Base
  include UI
  attr_accessor :viewport
  def initialize
    super()
    @viewport = Viewport.create(:main, 25)
  end

  def update
    return unless super
    @running = false if Input.trigger?(:B)
  end

  def dispose
    super
    @viewport.dispose
  end
end

Tester.start(Dummy)
