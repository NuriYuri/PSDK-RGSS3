module GamePlay
  # Class that display the Party Menu interface and manage user inputs
  #
  # This class has several modes
  #   - :map => Used to select a Pokemon in order to perform stuff
  #   - :menu => The normal mode when opening this interface from the menu
  #   - :battle => Select a Pokemon to send to battle
  #   - :item => Select a Pokemon in order to use an item on it (require extend data : hash)
  #   - :hold => Give an item to the Pokemon (requires extend data : item_id)
  #
  # This class can also show an other party than the player party,
  # the party paramter is an array of Pokemon upto 6 Pokemon
  class Party_Menu2 < Base
    # Return data of the Party Menu
    # @return [Integer]
    attr_accessor :return_data
    # Return the skill process to call
    # @return [Array(Proc, PFM::Pokemon, PFM::Skill), Proc, nil]
    attr_accessor :call_skill_process
    # Selector Rect info
    # @return [Array]
    SelectorRect = [[0, 0, 132, 52], [0, 64, 132, 52]]
    # Create a new Party_Menu
    # @param party [Array<PFM::Pokemon>] list of Pokémon in the party
    # @param mode [Symbol] :map => from map (select), :menu => from menu, :battle => from Battle, :item => Use an item, :hold => Hold an item, :choice => processing a choice related proc (do not use)
    # @param extend_data [Integer, Hash] extend_data informations
    def initialize(party, mode = :map, extend_data = nil)
      super()
      @move = -1
      @return_data = -1
      # Scene mode
      # @type [Symbol]
      @mode = mode
      # Displayed party
      # @type [Integer, Hash, nil]
      @extend_data = extend_data
      @index = 0
      # @type [Array<PFM::Pokemon>]
      @party = party
      @counter = 0 #  Used by the selector
      @intern_mode = :normal # :normal, :move_pokemon, :move_item, :choose_move_pokemon, :choose_move_item
      # Scene viewport
      # @type [LiteRGSS::Viewport]
      @viewport = Viewport.create(:main, 500)
      create_background
      create_team_buttons
      create_frames #  Must be after team buttons to ensure the black frame to work
      create_selector
      create_ctrls
      create_win_text
      init_win_text
      # Telling the B action the user is seeing a choice and make it able to cancel the choice
      # @type [PFM::Choice_Helper]
      @choice_object = nil
      # Running state of the scene
      # @type [Boolean]
      @running = true
    end

    # Create the background sprite
    def create_background
      # Scene background
      # @type [LiteRGSS::Sprite]
      @background = Sprite.new(@viewport).set_bitmap('team/Fond', :interface)
    end

    # Create the frame sprites
    def create_frames
      # @type [LiteRGSS::Sprite]
      @black_frame = Sprite.new(@viewport) #  Get the Blackn ^^
      # Scene frame
      # @type [LiteRGSS::Sprite]
      @frame = Sprite.new(@viewport).set_bitmap($options.language == 'fr' ? 'team/FrameFR' : 'team/FrameEN', :interface)
    end

    # Create the team buttons
    def create_team_buttons
      # Team button list
      # @type [Array<UI::TeamButton>]
      @team_buttons = Array.new(@party.size) do |i|
        btn = UI::TeamButton.new(@viewport, i)
        btn.data = @party[i]
        next(btn)
      end
    end

    # Create the selector
    def create_selector
      # Scene selector
      # @type [LiteRGSS::Sprite]
      @selector = Sprite.new(@viewport).set_bitmap('team/Cursors', :interface)
      @selector.src_rect.set(*SelectorRect[0])
      update_selector_coordinates
    end

    # Create the control buttons
    def create_ctrls
      # Scene Control buttons
      # @type [Array<UI::TeamCTRLButton>]
      @ctrl = Array.new(4) { |i| UI::TeamCTRLButton.new(@viewport, i) }
    end

    # Create the text window (info to the player)
    def create_win_text
      # Scene Text window (info)
      # @type [UI::SpriteStack]
      @winText = UI::SpriteStack.new(@viewport)
      @winText.push(0, 217, 'team/Win_Txt')
      # Real text info
      # @type [LiteRGSS::Text]
      @text_info = @winText.add_text(2, 220, 238, 15, nil.to_s, color: 9)
      # Auto texts
      case @mode
      when :battle
        show_winText(_get(20, 22))
      when :item
        show_winText(_get(23, 24))
      when :hold
        show_winText(_get(23, 23))
      when :map
        show_winText(_get(23, 17))
      end
    end

    # Initialize the win_text according to the mode
    def init_win_text
      case @mode
      when :map, :battle
        return @text_info.text = _get(23, 17)
      when :hold
        return @text_info.text = _get(23, 23)
      when :item
        if @extend_data
          extend_data_button_update
          return @text_info.text = _get(23, 24)
        end
      end
      @winText.visible = false
    end

    # Function that update the team button when extend_data is correct
    def extend_data_button_update
      if (_proc = @extend_data[:on_pokemon_choice])
        apt_detect = (@extend_data[:open_skill_learn] or @extend_data[:stone_evolve])
        @team_buttons.each do |btn|
          btn.show_item_name
          v = @extend_data[:on_pokemon_choice].call(btn.data)
          if apt_detect
            c = (v ? 1 : v == false ? 2 : 3)
            v = (v ? 143 : v == false ? 144 : 142)
          else
            c = (v ? 1 : 2)
            v = (v ? 140 : 141)
          end
          btn.item_text.load_color(c).text = _parse(22, v)
        end
      end
    end

    # Globaly update the scene
    def update
      update_selector
      return unless super
      update_mouse_ctrl
      return action_A if Input.trigger?(:A)
      return action_X if Input.trigger?(:X)
      return action_Y if Input.trigger?(:Y)
      return action_B if Input.trigger?(:B)
      update_selector_move
    end

    # Update the selector
    def update_selector
      @counter += 1
      if @counter == 60
        @selector.src_rect.set(*SelectorRect[1])
      elsif @counter >= 120
        @counter = 0
        @selector.src_rect.set(*SelectorRect[0])
      end
    end

    # Action triggered when A is pressed
    def action_A
      #return $game_system.se_play($data_system.buzzer_se) if @page_id
      #$game_system.se_play($data_system.decision_se)
      case @mode
      when :menu
        action_A_menu
      end
    end

    # Action triggered when B is pressed
    def action_B
      $game_system.se_play($data_system.decision_se)
      # Cancel choice attempt
      return @choice_object.cancel if @choice_object
      # Returning to normal mode
      if @intern_mode != :normal
        hide_winText
        hide_item_name
        @team_buttons[@move].selected = false if @move != -1
        @move = -1
        return @intern_mode = :normal
      end
    end

    # Action triggered when X is pressed
    def action_X
      return if @mode != :menu
      return $game_system.se_play($data_system.buzzer_se) if @intern_mode != :normal or @party.size <= 1
      show_winText(_get(23, 19))
      @intern_mode = :choose_move_pokemon
    end

    # Action triggered when Y is pressed
    def action_Y
      return if @mode != :menu
      return $game_system.se_play($data_system.buzzer_se) if @intern_mode != :normal or @party.size <= 1
      show_winText(_get(23, 20))
      @intern_mode = :choose_move_item
      show_item_name
    end

    # Action when A is pressed and the mode is menu
    def action_A_menu
      case @intern_mode
      when :choose_move_pokemon
        action_move_current_pokemon
      when :choose_move_item
        return $game_system.se_play($data_system.buzzer_se) if @team_buttons[@index].data.item_holding.zero?
        @team_buttons[@move = @index].selected = true
        @intern_mode = :move_item
        show_winText(_get(23, 22))
      when :move_pokemon
        process_switch
      when :move_item
        process_item_switch
      else
        $game_system.se_play($data_system.decision_se)
        return show_choice
      end
      $game_system.se_play($data_system.decision_se)
    end

    # Show the proper choice
    def show_choice
      case @mode
      when :menu
        show_menu_mode_choice
      when :choice
        show_choice_mode_choice
      when :battle
        show_battle_mode_choice
      when :item
        show_item_mode_choice
      when :hold
        show_hold_mode_choice
      else
        show_map_mode_choice
      end
    end

    # Select the current pokemon to move with an other pokemon
    def action_move_current_pokemon
      return if @party.size <= 1
      @team_buttons[@move = @index].selected = true
      @intern_mode = :move_pokemon
      show_winText(_get(23, 21))
    end

    # Array of actions to do according to the pressed button
    Actions = %i[action_A action_X action_Y action_B]

    # Update the mouse interaction with the ctrl buttons
    def update_mouse_ctrl
      win_text = @winText.visible
      if Mouse.trigger?(:left)
        @ctrl.each_with_index do |sp, i|
          next if win_text and i != 3
          sp.set_press(sp.simple_mouse_in?)
        end
      elsif Mouse.released?(:left)
        @ctrl.each_with_index do |sp, i|
          next if win_text and i != 3
          send(Actions[i]) if sp.simple_mouse_in?
          sp.set_press(false)
        end
      end
    end

    # Update the movement of the Cursor
    def update_selector_move
      party_size = @team_buttons.size
      index2 = @index % 2
      if Input.trigger?(:DOWN)
        next_index = @index + 2
        next_index = index2 if next_index >= party_size
        update_selector_coordinates(@index = next_index)
      elsif Input.trigger?(:UP)
        next_index = @index - 2
        if next_index < 0
          next_index += 6
          next_index -= 2 while next_index >= party_size
        end
        update_selector_coordinates(@index = next_index)
      elsif index_changed(:@index, :LEFT, :RIGHT, party_size - 1)
        update_selector_coordinates
      else
        update_mouse_selector_move
      end
    end

    # Update the movement of the selector with the mouse
    def update_mouse_selector_move
      return unless Mouse.moved or Mouse.trigger?(:left) # Safety preventing index conflict
      @team_buttons.each_with_index do |btn, i|
        next unless btn.simple_mouse_in?
        update_selector_coordinates(@index = i) if @index != i
        action_A if Mouse.trigger?(:left)
        return true
      end
    end

    # Update the selector coordinates
    def update_selector_coordinates(*)
      btn = @team_buttons[@index]
      @selector.set_position(btn.x + 3, btn.y + 3)
    end

    # Show the winText
    # @param str [String] String to put in the Win Text
    def show_winText(str)
      @text_info.text = str
      @winText.visible = true
    end

    # Hide the winText
    def hide_winText
      @winText.visible = false
    end

    # Show the item name
    def show_item_name
      @team_buttons.each(&:show_item_name)
    end

    # Hide the item name
    def hide_item_name
      @team_buttons.each(&:hide_item_name)
    end

    # Process the switch between two pokemon
    def process_switch
      return $game_system.se_play($data_system.buzzer_se) if @move == @index
      tmp = @team_buttons[@move].data
      @team_buttons[@move].selected = false
      @party[@move] = @team_buttons[@move].data = @team_buttons[@index].data
      @party[@index] = @team_buttons[@index].data = tmp
      @move = -1
      hide_winText
      @intern_mode = :normal
    end

    # Process the switch between the items of two pokemon
    def process_item_switch
      return $game_system.se_play($data_system.buzzer_se) if @move == @index
      tmp = @team_buttons[@move].data.item_holding
      # @type [PFM::Pokemon]
      pokemon = @team_buttons[@move].data
      pokemon.item_holding = @team_buttons[@index].data.item_holding
      if pokemon.form_calibrate # Form adjustment
        @team_buttons[@move].refresh
        display_message(_parse(22, 157, ::PFM::Text::PKNAME[0] => pokemon.given_name))
      end
      @team_buttons[@move].refresh
      pokemon = @team_buttons[@index].data
      pokemon.item_holding = tmp
      if pokemon.form_calibrate # Form adjustment
        @team_buttons[@index].refresh
        display_message(_parse(22, 157, ::PFM::Text::PKNAME[0] => pokemon.given_name))
      end
      @team_buttons[@index].refresh
      @team_buttons[@move].selected = false
      @move = -1
      hide_winText
      hide_item_name
      @intern_mode = :normal
    end

    # Show the black frame for the currently selected Pokemon
    def show_black_frame
      @black_frame.set_bitmap("team/dark#{@index + 1}", :interface)
      @black_frame.visible = true
    end

    # Hide the black frame for the currently selected Pokemon
    def hide_black_frame
      @black_frame.visible = false
    end

    # Show the choice when party is in mode :menu
    def show_menu_mode_choice
      show_black_frame
      # @type [PFM::Pokemon]
      pokemon = @party[@index]
      choices = PFM::Choice_Helper.new(Window_Choice::But, true, 999)
      unless pokemon.egg?
        pokemon.skills_set.each_with_index do |skill, i|
          if skill and (skill.map_use > 0 or ::PFM::SkillProcess[skill.id])
            choices.register_choice(skill.name, i, on_validate: method(:use_pokemon_skill), color: get_skill_color)
          end
        end
      end

      choices
        .register_choice(_get(23, 4), on_validate: method(:launch_summary)) # Summary
        .register_choice(_get(23, 8), on_validate: method(:action_move_current_pokemon), disable_detect: proc { @party.size <= 1 }) # Move
      unless pokemon.egg?
        choices
          .register_choice(_get(23, 146), on_validate: method(:give_item)) # Give
          .register_choice(_get(23, 147), on_validate: method(:take_item), disable_detect: method(:current_pokemon_has_no_item)) # Take
      end
      # choices.register_choice(_get(23, 138), on_validate: method(:hide_winText)) # Cancel
      show_winText(_parse(23, 30, ::PFM::Text::PKNICK[0] => pokemon.given_name))
      x, y = get_choice_coordinates(choices)
      choice = choices.display_choice(@viewport, x, y, 72, choices, on_update: method(:update_menu_choice))
      hide_winText if choice == 999
      hide_black_frame
    end

    # Update the scene during a choice
    # @param choices [PFM::Choice_Helper] choice interface to be able to cancel
    def update_menu_choice(choices)
      @choice_object = choices
      update_selector
      update_mouse_ctrl
      @choice_object = nil
    end

    # Return the choice coordinates according to the current selected Pokemon
    # @param choices [PFM::Choice_Helper] choice interface to be able to cancel
    # @return [Array(Integer, Integer)]
    def get_choice_coordinates(choices)
      choice_height = 16
      height = choices.size * choice_height
      max_height = 217
      but_x = @team_buttons[@index].x + 53
      but_y = @team_buttons[@index].y + 32
      if but_y + height > max_height
        but_y -= (height - choice_height)
        but_y += choice_height while but_y < 0
      end
      return but_x, but_y
    end

    # Action of using a move of the current Pokemon
    # @param move_index [Integer] index of the move in the Pokemon moveset
    def use_pokemon_skill(move_index)
      # @type [PFM::Pokemon]
      pokemon = @party[@index]
      # @type [PFM::Skill]
      skill = pokemon.skills_set[move_index]
      if (@call_skill_process = PFM::SkillProcess[skill.id])
        if (type = @call_skill_process.call(pokemon, nil, true))
          if type == true
            @call_skill_process.call(pokemon, skill)
            @call_skill_process = nil
          elsif type == :choice
            @mode = :choice
            @return_data = @index
            show_winText(_get(23, 17))
            return
          elsif type == :block
            display_message(_parse(22, 108))
            hide_winText
            return
          end
        else
          @call_skill_process = [@call_skill_process, pokemon, skill]
        end
      else
        $game_temp.common_event_id = skill.map_use
      end
      hide_winText
      $game_variables[Yuki::Var::Party_Menu_Sel] = @index
      @running = false
    end

    # Action of launching the Pokemon Summary
    # @param mode [Symbol] mode used to launch the summary
    # @param extend_data [Hash, nil] the extended data used to launch the summary
    def launch_summary(mode = :view, extend_data = nil)
      hide_winText
      call_scene(Sumary, @party[@index], @viewport.z, mode, @party, extend_data)
    end

    # Action of giving an item to the Pokemon
    # @param item2 [Integer] id of the item to give
    # @note if item2 is -1 it'll call the Bag interface to get the item
    def give_item(item2 = -1)
      # @type [PFM::Pokemon]
      pokemon = @party[@index]
      if item2 == -1
        @__result_process = proc { |scene| item2 = scene.return_data }
        call_scene(Bag, :hold)
      end
      return hide_winText if item2 == -1
      item1 = pokemon.item_holding
      give_item_message(item1, item2, pokemon)
      give_item_update_state(item1, item2, pokemon)
      @team_buttons[@index].refresh
      return hide_winText unless pokemon.form_calibrate # Form adjustment
      @team_buttons[@index].refresh
      form_change_message(pokemon)
      hide_winText
    end

    # Display the give item message
    # @param item1 [Integer] taken item
    # @param item2 [Integer] given item
    # @param pokemon [PFM::Pokemon] Pokemong getting the item
    def give_item_message(item1, item2, pokemon)
      if item1 != 0 and item1 != item2
        display_message(_parse(22, 91, ::PFM::Text::ITEM2[0] => pokemon.item_name, ::PFM::Text::ITEM2[1] => ::GameData::Item.name(item2)))
      elsif item1 != item2
        display_message(_parse(22, 90, ::PFM::Text::ITEM2[0] => ::GameData::Item.name(item2)))
      end
    end
    # Update the bag and pokemon state when giving an item
    # @param item1 [Integer] taken item
    # @param item2 [Integer] given item
    # @param pokemon [PFM::Pokemon] Pokemong getting the item
    def give_item_update_state(item1, item2, pokemon)
      pokemon.item_holding = item2
      $bag.remove_item(item2, 1)
      $bag.add_item(item1, 1) if item1 != 0
    end

    # Action of taking the item from the Pokemon
    def take_item
      hide_winText
      # @type [PFM::Pokemon]
      pokemon = @party[@index]
      item = pokemon.item_holding
      $bag.add_item(item, 1)
      pokemon.item_holding = 0
      @team_buttons[@index].data = pokemon
      @team_buttons[@index].refresh
      display_message(_parse(23, 78, ::PFM::Text::PKNICK[0] => pokemon.given_name, ::PFM::Text::ITEM2[1] => ::GameData::Item.name(item)))
      return hide_winText unless pokemon.form_calibrate # Form ajustment
      @team_buttons[@index].refresh
      form_change_message(pokemon)
      hide_winText
    end

    # Form change message when item is taken or given
    # @note : Also update interface state
    # @param pokemon [PFM::Pokemon] Pokemon that change form
    def form_change_message(pokemon)
      @team_buttons[@index].data = pokemon
      display_message(_parse(22, 157, ::PFM::Text::PKNAME[0] => pokemon.given_name))
    end

    # Method telling if the Pokemon has no item or not
    # @return [Boolean]
    def current_pokemon_has_no_item
      @party[@index].item_holding <= 0
    end

    # Return the skill color
    # @return [Integer]
    def get_skill_color
      return 1
    end

    # Show the choice when the party is in mode :battle
    def show_battle_mode_choice
      # @type [PFM::Pokemon]
      pokemon = @party[@index]
      choices = PFM::Choice_Helper.new(Window_Choice::But, true, 999)
      choices
        .register_choice(_get(20, 26), on_validate: method(:on_send_pokemon)) # Send
        .register_choice(_get(23, 4), on_validate: method(:launch_summary)) # Summary
      show_winText(_parse(23, 30, ::PFM::Text::PKNICK[0] => pokemon.given_name))
      x, y = get_choice_coordinates(choices)
      choices.display_choice(@viewport, x, y, 72, choices, on_update: method(:update_menu_choice))
    end

    # When the player want to send a specific Pokemon to battle
    def on_send_pokemon
      # @type [PFM::Pokemon]
      pokemon = @party[@index]
      if pokemon.egg?
        display_message(_parse(20, 34))
      elsif pokemon.dead?
        display_message(_parse(20, 33, ::PFM::Text::PKNICK[0] => pokemon.given_name))
      elsif @index < $game_temp.vs_type
        display_message(_parse(20, 32, ::PFM::Text::PKNICK[0] => pokemon.given_name))
      else
        @return_data = @index
        @running = false
      end
    end

    # Show the choice when the party is in mode :choice
    def show_choice_mode_choice
      # @type [PFM::Pokemon]
      pokemon = @party[@index]
      choices = PFM::Choice_Helper.new(Window_Choice::But, true, 999)
      choices
        .register_choice(_get(23, 209), on_validate: method(:on_skill_choice)) # Select
        .register_choice(_get(23, 4), on_validate: method(:launch_summary)) # Summary
        .register_choice(_get(23, 1), on_validate: method(:hide_winText)) # Cancel
      show_winText(_parse(23, 30, ::PFM::Text::PKNICK[0] => pokemon.given_name))
      x, y = get_choice_coordinates(choices)
      choice = choices.display_choice(@viewport, x, y, 72, choices, on_update: method(:update_menu_choice))
      show_winText(_get(23, 17)) if choice != 0
    end

    # Event that triggers when the player choose on which pokemon to apply the move
    def on_skill_choice
      # @type [PFM::Pokemon]
      pokemon = @party[@index]
      @call_skill_process.call(pokemon, nil)
      @call_skill_process = nil
      @mode = :menu
      @index = @return_data
      @return_data = -1
    end

    # Show the choice when the party is in mode :item
    def show_item_mode_choice
      # @type [PFM::Pokemon]
      pokemon = @party[@index]
      choices = PFM::Choice_Helper.new(Window_Choice::But, true, 999)
      choices
        .register_choice(_get(23, 209), on_validate: method(:on_item_use_choice)) # Select
        .register_choice(_get(23, 4), on_validate: method(:launch_summary)) # Summary
        .register_choice(_get(23, 1), on_validate: method(:hide_winText)) # Cancel
      show_winText(_parse(23, 30, ::PFM::Text::PKNICK[0] => pokemon.given_name))
      x, y = get_choice_coordinates(choices)
      choice = choices.display_choice(@viewport, x, y, 72, choices, on_update: method(:update_menu_choice))
      show_winText(_get(23, 24)) if choice != 0
    end

    # Event that triggers when the player choose on which pokemon to use the item
    def on_item_use_choice
      # @type [PFM::Pokemon]
      pokemon = @party[@index]
      if @extend_data[:on_pokemon_choice].call(pokemon)
        if @extend_data[:on_pokemon_use]
          @extend_data[:on_pokemon_use].call(pokemon)
          @return_data = @index
          @running = false
        elsif @extend_data[:open_skill]
          launch_summary(:skill, @extend_data)
          if @extend_data[:skill_selected]
            @return_data = @index
            @running = false
          end
        elsif @extend_data[:open_skill_learn]
          scene = Skill_Learn.new(pokemon, @extend_data[:open_skill_learn])
          scene.main
          @return_data = @index if scene.learnt
          @running = false
        elsif @extend_data[:action_to_push]
          @return_data = @index
          @running = false
        end
      else
        display_message(_parse(22, 108))
      end
    end

    # Show the choice when the party is in mode :hold
    def show_hold_mode_choice
      # @type [PFM::Pokemon]
      pokemon = @party[@index]
      choices = PFM::Choice_Helper.new(Window_Choice::But, true, 999)
      choices
        .register_choice(_get(23, 146), on_validate: method(:on_item_give_choice)) # Select
        .register_choice(_get(23, 4), on_validate: method(:launch_summary)) # Summary
        .register_choice(_get(23, 1), on_validate: method(:hide_winText)) # Cancel
      show_winText(_parse(23, 30, ::PFM::Text::PKNICK[0] => pokemon.given_name))
      x, y = get_choice_coordinates(choices)
      choice = choices.display_choice(@viewport, x, y, 72, choices, on_update: method(:update_menu_choice))
      show_winText(_get(23, 23)) if choice != 0
    end

    # Event that triggers when the player choose on which pokemon to give the item
    def on_item_give_choice
      give_item(@extend_data)
      @running = false
    end

    # Show the choice when the party is in mode :map
    def show_map_mode_choice
      # @type [PFM::Pokemon]
      pokemon = @party[@index]
      choices = PFM::Choice_Helper.new(Window_Choice::But, true, 999)
      choices
        .register_choice(_get(23, 209), on_validate: method(:on_map_choice)) # Select
        .register_choice(_get(23, 4), on_validate: method(:launch_summary)) # Summary
        .register_choice(_get(23, 1), on_validate: method(:hide_winText)) # Cancel
      show_winText(_parse(23, 30, ::PFM::Text::PKNICK[0] => pokemon.given_name))
      x, y = get_choice_coordinates(choices)
      choice = choices.display_choice(@viewport, x, y, 72, choices, on_update: method(:update_menu_choice))
      show_winText(_get(23, 17)) if choice != 0
    end

    # Event that triggers when the player has choosen a Pokemon
    def on_map_choice
      $game_variables[Yuki::Var::Party_Menu_Sel] = @index
      @running = false
    end

    def dispose
      super
      @viewport.dispose
    end
  end
end


#Tester.start(GamePlay::Party_Menu2, team = Array.new(1 + rand(6)) { PFM::Pokemon.new(1 + rand(500), 5) }, :menu)
team = Array.new(2) { PFM::Pokemon.new(487, 100) }
team[0].item_holding = 112#rand(500) + 1
team[0].form_calibrate
4.times do team << PFM::Pokemon.new(rand(493)+1, 5+rand(95)) end
Tester.start(GamePlay::Party_Menu2, team, :menu)
