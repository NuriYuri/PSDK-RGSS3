module GamePlay
  # Class that shows the Pokedex
  class Dex < Base
    # Text format for the name
    NameStr="%03d - %s"
    # Text format for the weight
    WeightStr="Poids : %.2f Kg"
    # Text format for the height
    HeightStr="Taille : %.2f m"
    include UI
    # Create a new Pokedex interface
    # @param page_id [Integer, false] id of the page to show
    def initialize(page_id = false)
      super()
      @viewport = Viewport.create(:main, 0)
      @background = Sprite.new(@viewport).set_bitmap("Fond", :pokedex)
      # Liste
      @list = Array.new(6) do |i| DexButton.new(@viewport, i) end
      @pokemonlist = PFM::Pokemon.new(0, 1)
      @arrow = Sprite.new(@viewport).set_bitmap("Arrow", :pokedex).set_position(127, 0)
      @arrowd = 1
      # Scrool
      @scrollbar = Sprite.new(@viewport).set_bitmap("Scroll", :pokedex)
        .set_position(309, 36)
      @scrollbut = Sprite.new(@viewport).set_bitmap("But_Scroll", :pokedex)
        .set_position(308, 41)
      # Frame
      @frame = Sprite.new(@viewport)
      @pokeface = DexWinSprite.new(@viewport)
      # Num generation
      @seen_got = DexSeenGot.new(@viewport)
      # Info
      @pokemon_info = DexWinInfo.new(@viewport)
      @pokemon_descr = Text.new(0, @viewport, 11, 153, 298, 16, nil.to_s).load_color(10)
      # Lieu
      @pokemon_zone = DexWinMap.new(@viewport)
      @state = page_id ? 1 : 0
      @page_id = page_id
      @ctrl = Array.new(4) do |i| DexCTRLButton.new(@viewport, i) end
      generate_selected_pokemon_array(page_id)
      generate_pokemon_object
      change_state(@state)
      Mouse.wheel = 0
    end
    
    # Update the interface
    def update
      @background.set_origin((@background.ox - 0.5) % 16, (@background.oy - 0.5) % 16)
      update_arrow if @arrow.visible
      return unless super
      update_mouse_ctrl
      return action_A if Input.trigger?(:A)
      return action_X if Input.trigger?(:X)
      return action_Y if Input.trigger?(:Y)
      return action_B if Input.trigger?(:B)
      if @state == 0
        max_index = @selected_pokemons.size - 1
        if index_changed(:@index, :UP, :DOWN, max_index)
          update_index
        elsif index_changed!(:@index, :LEFT, :RIGHT, max_index)
          9.times { index_changed!(:@index, :LEFT, :RIGHT, max_index) }
          update_index
        elsif Mouse.wheel != 0
          @index = (@index - Mouse.wheel) % (max_index + 1)
          Mouse.wheel = 0
          update_index
        end
      end
    end
    
    # Update the arrow animation
    def update_arrow
      return if Graphics.frame_count % 15 != 0
      @arrow.x += @arrowd
      @arrowd = 1 if @arrow.x <= 127
      @arrowd = -1 if @arrow.x >= 129
    end
    
    # Update the index when changed
    def update_index
      @pokemon.id = @selected_pokemons[@index]
      @pokeface.data = @pokemon
      update_list(true)
    end
    
    # Action triggered when A is pressed
    def action_A
      return $game_system.se_play($data_system.buzzer_se) if @page_id
      $game_system.se_play($data_system.decision_se)
      change_state(@state + 1) if @state < 2
    end
    
    # Action triggered when B is pressed
    def action_B
      $game_system.se_play($data_system.decision_se)
      return @running = false if @state == 0 or @page_id
      change_state(@state - 1) if @state > 0
    end
    
    # Action triggered when X is pressed
    def action_X
      return if @state > 1 
      return $game_system.se_play($data_system.buzzer_se) if @page_id
      return $game_system.se_play($data_system.buzzer_se) #Non programmé
    end
    
    # Action triggered when Y is pressed
    def action_Y
      return if @state > 1
      return $game_system.se_play($data_system.buzzer_se) if @state == 0
      $game_system.cry_play(@pokemon.id) if @state == 1
    end
    
    # Array of actions to do according to the pressed button
    Actions = [:action_A, :action_X, :action_Y, :action_B]
    # Update the mouse interaction with the ctrl buttons
    def update_mouse_ctrl
      if Mouse.trigger?(:left)
        @ctrl.each do |sp|
          sp.set_press(sp.simple_mouse_in?)
        end
      elsif Mouse.released?(:left)
        @ctrl.each_with_index do |sp, i|
          if sp.simple_mouse_in?
            send(Actions[i])
          end
          sp.set_press(false)
        end
      end
    end
    
    # Change the state of the Interface
    # @param state [Integer] the id of the state
    def change_state(state)
      @state = state
      @ctrl.each { |sp| sp.set_state(state) }
      @frame.set_bitmap(state == 1 ? "FrameInfos" : "Frame", :pokedex)
      @pokeface.data = @pokemon if(@pokeface.visible = state != 2)
      @arrow.visible = @seen_got.visible = state == 0
      @pokemon_info.visible = @pokemon_descr.visible = state == 1
      if @pokemon_descr.visible
        @pokemon_descr.multiline_text = ::GameData::Pokemon.descr(@pokemon.id)
        @pokemon_info.data = @pokemon
      end
      @pokemon_zone.data = @pokemon if(@pokemon_zone.visible = state == 2)
      update_list(state == 0)
    end
    
    # Update the button list
    # @param visible [Boolean]
    def update_list(visible)
      @scrollbar.visible = @scrollbut.visible = visible
      @scrollbut.y = 41 + 151 * @index / @selected_pokemons.size
      base_index = calc_base_index
      @list.each_with_index do |el, i|
        next unless el.visible = visible
        pos = base_index + i
        id = @selected_pokemons[pos]
        next(el.visible = false) unless id and pos >= 0
        if el.selected = (pos == @index)
          @arrow.y = el.y + 11
        end
        @pokemonlist.id = id
        el.data = @pokemonlist
      end
    end
    
    # Calculate the base index of the list
    # @return [Integer]
    def calc_base_index
      return -1 if @selected_pokemons.size < 5
      if @index >= 2
        return @index - 2
      elsif @index < 2
        return -1
      end
    end
    
    # Generate the selected_pokemon array
    # @param page_id [Integer, false] see initialize
    def generate_selected_pokemon_array(page_id)
      if $pokedex.national?
        1.step($game_data_pokemon.size-1) do |i|
          @selected_pokemons << i if $pokedex.has_seen?(i)
        end
      else
        selected_pokemons = Array.new
        1.step($game_data_pokemon.size-1) do |i|
          selected_pokemons << i if $pokedex.has_seen?(i) and ::GameData::Pokemon.id_bis(i) > 0
        end
        selected_pokemons.sort! do |a, b| ::GameData::Pokemon.id_bis(a) <=> ::GameData::Pokemon.id_bis(b) end
        @selected_pokemons = selected_pokemons
      end
      @selected_pokemons << 0 if @selected_pokemons.size == 0
      # Index ajustment
      if(page_id)
        @index = @selected_pokemons.index(page_id)
        unless @index
          @selected_pokemons << page_id
          @index = @selected_pokemons.size - 1
        end
        #@index -= 1
      else
        @index = 0
      end
    end
    
    # Generate the Pokemon Object
    def generate_pokemon_object
      @pokemon = PFM::Pokemon.new(@selected_pokemons[@index].to_i,1)
      def @pokemon.pokedex_name
        sprintf(GamePlay::Dex::NameStr, self.id, self.name)
      end
      def @pokemon.pokedex_species
        ::GameData::Pokemon.species(self.id)
      end
      def @pokemon.pokedex_weight
        sprintf(GamePlay::Dex::WeightStr, self.weight)
      end
      def @pokemon.pokedex_height
        sprintf(GamePlay::Dex::HeightStr, self.height)
      end
    end
    
    # Dispose the interface
    def dispose
      super
      @viewport.dispose
    end
  end
end

module UI
  # Dex sprite that show the Pokemon infos
  class DexButton < SpriteStack
    # Create a new dex button
    # @param viewport [LiteRGSS::Viewport]
    # @param i [Integer] index of the sprite in the viewport
    def initialize(viewport, i)
      super(viewport, 0, 0, default_cache: :pokedex)
      push(147, 62, "But_List")
      push(266, 71, "Catch") #> Should always be the second
      push(148 + 16, 61 + 16, nil, type: PokemonIconSprite)
      add_text(182, 63, 116, 16, :id_text3, type: SymText, color: 10)
      add_text(182, 78, 116, 16, :name, type: SymText, color: 10)
      push(147, 62, "But_ListShadow") #> Should always be the last
      @x = 147
      @y = 62
      self.set_position(i == 0 ? 147 : 163, @y - 40 + i * 40)
    end
    # Change the data
    def data=(pokemon)
      super(pokemon)
      @stack[1].visible = ($pokedex.has_captured?(pokemon.id))
    end
    # Set the button in selected state or not
    def selected=(value)
      @stack.last.visible = !value
      self.set_position(value ? 147 : 163, @y)
    end
  end
end

def init_dex
  200.times do |i|
    $pokedex.mark_seen(i*2)
    $pokedex.mark_captured(i*2) if rand(2) == 0
  end
end

Tester.start(GamePlay::Dex)