
class Scene_Test1 < GamePlay::Base
  include UI
  attr_accessor :stack
  def initialize
    super()
    @viewport = Viewport.create(:main, 25)
    @background = Sprite.new(@viewport).set_bitmap("back_building", :battleback)
    @background.z = 0
    @stack = SpriteStack.new(@viewport)
    (text = @stack.add_text(0, 120 - 8, 320, 16, "J'ai été ajouté :D", 1, 1, color: 1)).z = 1
    @stack.add_text(0, 0, 320, 16, :name, color: 2, type: SymText)
    @stack.data = PFM::Pokemon.new(151, 5)
    @dragger = GamePlay::DragSprite.new
    @dragger.add(@background, 
      update_drag: proc do |sprite| 
        #> Interdiction de dépasser certaines bornes
        sprite.x = 0 if sprite.x < 0
        sprite.y = 0 if sprite.y < 0
        puts "Sprite dragged to #{sprite.x} #{sprite.y}"
      end
     )
    @dragger.add(@stack, update_drag: proc { |sprite| puts "Text dragged to #{sprite.x} #{sprite.y}"})
    @btn1 = UI::Button.new(@viewport, 120, 16, "button_120", "Test bouton 1").on_click { |button| puts "Button 1 clicked" }
    @btn2 = UI::Button.new(@viewport, 120, 16 + 19, "button_120", :name).on_click { |button| puts "Button 2 clicked" }
    @btn2.data = @stack.data
  end
  
  def update
    return unless super
    return if @btn1.update
    return if @btn2.update
    @dragger.update
    @running = false if Input.trigger?(:B)
  end
  
  def dispose
    super
    @viewport.dispose
  end
  
  def tester
    @stack.data = PFM::Pokemon.new(rand(500), 5)
  end
end

Tester.start(Scene_Test1)