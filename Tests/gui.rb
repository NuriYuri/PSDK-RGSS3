module GamePlay
  class GUI < Base
    def initialize
      super() # Call the Base initialize method ignoring the argument of the current initialize
      @viewport = Viewport.create(:main, 1000) # Viewport that holds the sprites of the interface
      @manager = ::GUI::Manager.new(@viewport, self, 4, 7)
      label = @manager.add_label(:label1, "Un label utilisé en titre", width: 4)
      label.load_color(9)
      label.align = 1
      @manager.add_input(:input1, width: 2) do |name, input|
        puts "Nouveau contenu de #{name} : #{input.text}"
      end
      @manager.add_button(:checkbox1, "Une checkbox", type: ::GUI::CheckBox, width: 2) do |name, button|
        puts "Nouvel état de #{name} : #{button.checked ? 'checked' : 'unchecked'}"
      end
      @manager.add_button(:button1, "Valider", width: 4, on_click: :validate)
    end
    
    # Updates the interface
    def update
      return unless super # The update method from base tells if the update can continue or not (message display)
      @manager.update
    end
    
    def validate(name, button)
      checked = @manager.buttons[:checkbox1].checked
      text = @manager.inputs[:input1].text
      puts "Validé !\nEtats :\n- checkbox = #{checked ? 'checked' : 'unchecked'}\n- input = #{text}"
    end
    
    # Dispose the viewports of the interface
    def dispose
      super # Call the dispose from Base to dispose the messagebox
      @viewport.dispose #Under LiteRGSS Viewport#dispose disposes all the sprite it holds
    end
  end
end
# Tell the PSDK Test what to test
Tester.start(GamePlay::GUI)