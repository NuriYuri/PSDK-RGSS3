#encoding: utf-8
Dir.chdir('..')
Dir['Scripts-PSDK/*.rb'].each do |fn|
  File.open(fn, 'rb') do |f|
    data = f.read(f.size).force_encoding(Encoding::UTF_8)
    next if data[0, 7] == "#noyard"
    File.open(fn.sub('Scripts-PSDK/','yard/scripts/'), 'wb') do |f2|
      f2 << data
    end
  end
end