module Battle
  class Engine
    private
    # Update the state
    def _state_update
      @state[:act_follow_me] = nil
      @state[:enn_follow_me] = nil
      _state_decrease(:water_sport, 119)
      _state_decrease(:mud_sport, 121)
      _state_decrease(:trick_room, 122)
      _state_decrease(:gravity, 124)
      _state_decrease(:act_reflect, 132)
      _state_decrease(:enn_reflect, 133)
      _state_decrease(:act_light_screen, 136)
      _state_decrease(:enn_light_screen, 137)
      _state_decrease(:act_safe_guard, 140)
      _state_decrease(:enn_safe_guard, 141)
      _state_decrease(:act_mist, 144)
      _state_decrease(:enn_mist, 145)
      _state_decrease(:act_tailwind, 148)
      _state_decrease(:enn_tailwind, 149)
      _state_decrease(:act_lucky_chant, 152)
      _state_decrease(:enn_lucky_chant, 153)
      _state_decrease(:act_rainbow, 172)
      _state_decrease(:enn_rainbow, 173)
      _state_decrease(:act_firesea, 176)
      _state_decrease(:enn_firesea, 177)
      _state_decrease(:act_swamp, 180)
      _state_decrease(:enn_swamp, 181)
      _state_decrease(:wonder_room, 185)
      _state_decrease(:magic_room, 187)
    end
    # Update the sub state
    def _sub_state_update
      st = @state
      st[:klutz] = false
      st[:air_lock] = false
      each_battler do |pokemon|
        st[:klutz] = pokemon if _has_ability(pokemon, 116) #> Maladresse
        st[:air_lock] = pokemon if _has_abilities(pokemon, 109, 29) #> Air Lock ou Ciel Gris
      end
    end
    # Resets the state
    def _state_reset
      @state = {
        :last_critical_hit => 1, 
        :trick_room => 0, #> Compteur de Distortion
        :klutz => false, #> Etat de maladresse
        :air_lock => false, #> état de Air lock et ciel gris
        :last_type_modifier => 1,
        :act_light_screen => 0, #>Mur lumière
        :enn_light_screen => 0,
        :act_reflect => 0, #>Protection
        :enn_reflect => 0,
        :act_tailwind => 0, #>Vent arrière
        :enn_tailwind => 0,
        :act_lucky_chant => 0, #>Air Veinard
        :enn_lucky_chant => 0,
        :gravity => 0, #>Compteur de gravité
        :water_sport => 0, #>Tourniquet
        :mud_sport => 0, #>Lance-Boue
        :last_skill => nil, #>Dernière attaque (pour Photocopie)
        :knock_off => [], #>Sabotage
        :magic_room => 0, #> Zone Magique
        :wonder_room => 0, #> Zone Étrange
        :act_follow_me => nil, #> Par Ici / Poudre Fureur Actors
        :enn_follow_me => nil, #> Par Ici / Poudre Fureur Ennemis
        :act_spikes => 0, #> Picots coté actors
        :enn_spikes => 0, #> Picots coté ennemis
        :act_toxic_spikes => 0, #> Picots toxiques coté actors
        :enn_toxic_spikes => 0, #> Picots toxiques coté ennemis
        :act_stealth_rock => false, #> Piège de rock act
        :enn_stealth_rock => false, #> Piège de rock enn
        :act_sticky_web => false, #> Toile gluante act
        :enn_sticky_web => false, #> Toile gluante enn
        :act_safe_guard => 0, #> Rune protect
        :enn_safe_guard => 0, #> Rune protect
        :act_mist => 0, #> Rune protect
        :enn_mist => 0, #> Rune protect
        
        :act_rainbow => 0, #> Double la chance de status (A-eau-feu)
        :enn_rainbow => 0, #> Double la chance de status
        :act_swamp => 0, #> Réduit la vitesse par 2 (A-eau-herbe)
        :enn_swamp => 0, #> Réduit la vitesse par 2
        :act_firesea => 0, #> Inflige des dégats tous les tours (1/8)
        :enn_firesea => 0, #> Inflige des dégats tous les tours (1/8)
        
        :launcher_item => 0, #> Objet porté par le lanceur
        :target_item => 0, #>Objet porté par la cible
        :launcher_ability => 0, #>Talent du lanceur
        :target_ability => 0, #>Talent de la cible
        :ext_info => nil, #> Informations supplémentaires pour les attaques
        :none => nil
      }
    end
    # Decrease a battle state counter
    # @param symbol [Symbol] identifier of the state
    # @param text_id [Integer] id of the text in the text file 18
    def _state_decrease(symbol, text_id)
      if @state[symbol] > 0
        msg _parse(18, text_id) if((@state[symbol] -= 1) <= 0)
      end
    end
    # Remove a battle state
    # @param symbol [Symbol] identifier of the state
    # @param text_id [Integer] id of the text in the text file 18
    def _state_remove(symbol, text_id)
      value = @state[symbol]
      if value.class == Fixnum and value > 0
        @state[symbol] = 0
        msg _parse(18, text_id)
      elsif value == true
        @state[symbol] = false
        msg _parse(18, text_id)
      end
    end
  end
end
