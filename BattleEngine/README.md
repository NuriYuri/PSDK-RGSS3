# Battle Engine #
Cette section contient les scripts du futur moteur de combat de PSDK. 

Le but est de faire un moteur de combat dynamique aussi bien capable de gérer le 3v3, 6v6 que du 1v1v1v1v1v1v1v1v1v1v1 (si vous voyez ce que je veux dire :v)

# Notes (règles de combat observées dans X/Y)

# Message de fin :
Artwork affiché en venant de la droite + message

# Dégas

1. Animation
2. Son + perte pv
3. Message

## Combat double :

# Dégas : affichés en même temps sur toute les cibles.
# => Seule les barres des cible sont affichés

### Choix

Lors des choix, quand on passe au deuxième Pokémon, le bouton fuite se transforme en bouton retour, ceci permettant de faire une autre action.

### Switch de Pokémon
Le Pokémon switché (KO ou switché) ne peut pas être choisi, il est "mis en attente"
Quand il y a plusieurs switches (KO), le Pokémon N-Y choisi (Y >= 1, N étant le nombre de switch à faire dans le tour) propose le choix de la position où il est switché sur la banc, le Pokémon N lui est placé là où il y a un trou.

### Choix cible
Quand plusieurs cibles en même temps sont choisie, c'est toujours l'enemi le plus à gauche de la selection possible qui est pris (=> identifier en fonction de l'enemi le plus à gauche)
Exemple :
ex ec ec
ax ac u
Le deuxième e est la cible et ça touchera le deuxième a et les e de 2 à 3
