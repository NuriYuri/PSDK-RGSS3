module PFM
  # Pokemon that is fighting during the battle
  class Pokemon_Battler < ::PFM::Pokemon
    # Side in which the Pokemon is
    # @return [Integer]
    attr_accessor :side
    # Position of the Pokemon in the side
    # @return [Integer]
    attr_accessor :position
    # Order of the Pokemon in the "attack" actions
    # @return [Integer]
    attr_accessor :attack_order
    # ID of the skill the Pokemon wants to use
    # @return [Integer]
    attr_accessor :prepared_skill
    # ID of the last used skill
    # @return [Integer]
    attr_accessor :last_skill
    # Number of times the last skills has been used
    # @return [Integer]
    attr_accessor :last_skill_use_times
    # The battle Stage of the Pokemon [atk, dfe, spd, ats, dfs, eva, acc]
    # @return [Array(Integer, Integer, Integer, Integer, Integer, Integer, Integer)]
    attr_accessor :battle_stage
    # Number of turn the Pokemon has fought (before the exp is distributed)
    # @return [Integer]
    attr_accessor :battle_turns
    # The data of the items
    # @return [Array]
    attr_accessor :item_data
    # If the Pokemon is confused
    # @return [Boolean]
    attr_accessor :confuse
    # If the Pokemon is mega evolved
    # @return [Boolean]
    attr_accessor :mega_evolved
    # If the ability is affected by Mold Break
    # @return [Boolean]
    attr_reader :mold_breakable
    # If the ability has been mold breaked
    # @return [Boolean]
    attr_accessor :mold_broken
    # Number of turn the Pokemon is battling
    # @return [Integer]
    attr_accessor :nb_of_turn_here
    # Create a new Battle::Pokemon from a ::PFM::Pokemon
    def initialize(pokemon)
      # Copy of the Pokemon's informations
      pokemon.instance_variables.each do |iv|
        self.instance_variable_set(iv, pokemon.instance_variable_get(iv))
      end
      # Initialization of the Battler data
      @side = 0
      @position = nil
      @attack_order = 255
      @prepared_skill = 0
      @last_skill = 0
      @last_skill_use_times = 0
      @battle_stage = Array.new(7, 0)
      @battle_turns = 0
      @item_data = []
      @confuse = false
      @mega_evolved = false
      @mold_breakable = false # _detect_mold_breakable_ability
      @mold_broken = false
      @nb_of_turn_here = 0
    end
    # Compare the position of the pokemon with an other pokemon
    # @param other [PFM::Pokemon_Battler]
    # @return [Integer]
    def <=>(other)
      if(@positon and other.position)
        return @position <=> other.position
      elsif other.position
        return 1
      elsif @position
        return -1
      end
      return 0
    end
  end
end
