module Battle
  class Engine
    # If the Pokemon has an item
    # @param pokemon [PFM::Pokemon_Battler]
    # @param id [Integer] ID of the item in the database
    def _has_item(pokemon, id)
      return false if @state[:klutz] or pokemon.effect_embargo? or @state[:knock_off].include?(pokemon) or @state[:magic_room] > 0
      return pokemon.battle_item == id
    end
    # If the Pokemon has an item in the list
    # @param pokemon [PFM::Pokemon_Battler]
    # @param ids [Array<Integer>] list of IDs of item in the database
    def _has_items(pokemon, *ids)
      return false if @state[:klutz] or pokemon.effect_embargo? or @state[:knock_off].include?(pokemon) or @state[:magic_room] > 0
      return ids.include?(pokemon.battle_item)
    end
  end
end
