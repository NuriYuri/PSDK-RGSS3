module Battle
  class Engine
    private
    # If the Pokemon has the ability
    # @param pokemon [PFM::Pokemon_Battler]
    # @param id [Integer] id of the ability in the database
    # @return [Boolean]
    def _has_ability(pokemon, id)
      return false if pokemon.mold_broken or pokemon.effect_no_ability?
      return pokemon.ability == id
    end
    # If the Pokemon has an ability in the list
    # @param pokemon [PFM::Pokemon_Battler]
    # @param ids [Array<Integer>] the list of abilities
    # @return [Boolean]
    def _has_abilities(pokemon, *ids)
      return false if pokemon.mold_broken or pokemon.effect_no_ability?
      return ids.include?(pokemon.ability)
    end
    # If an enemy has the ability (returns the enemy)
    # @param pokemon [PFM::Pokemon_Battler] the pokemon that is against the enemies
    # @param id [Integer] id of the ability in the database
    # @return [PFM::Pokemon_Battler, nil] enemy if one has the ability
    def _enemy_has_ability(pokemon, id)
      each_enemy(pokemon) do |enemy|
        return enemy if _has_ability(enemy, id)
      end
      return nil
    end
  end
end
