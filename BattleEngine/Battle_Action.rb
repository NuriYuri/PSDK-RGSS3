module Battle
  # Class that describe an action in battle. This should help battle to run without any calibration from external script. (Online / Battle Royale)
  class Action
    # Action type
    # @return [Symbol]
    attr_reader :action_type
    # Priority of the action
    # @return [Integer]
    attr_reader :priority
    # If the action also include a mega evolve, == priority
    # @return [Integer]
    attr_reader :mega_priority
    # Return the launcher of the move
    # @return [PFM::Pokemon_Battler, nil]
    attr_reader :launcher
    # Return the target of the action
    # @return [PFM::Pokemon_Battler]
    attr_reader :target
    # Return the skill used in the action
    # @return [PFM::Skill, nil]
    attr_reader :skill
    # Create a new Battle_Action
    # @param action_type [Symbol] action type (:attack, :switch, :item, :flee)
    # @param target [Integer] target position
    # @param side [Integer] side id of the target
    # @param args [Array] specific properties
    #   For :attack :
    #     launcher [PFM::Pokemon_Battler] launcher of the attack
    #     skill [PFM::Skill] move used
    #     mega_evolve [Boolean] if the Pokemon mega evolve
    #   For :item
    #     item_id [Integer] id of the item in the database
    #     bag [Integer] id of the bag (to remove the item from the bag of the right trainer)
    #   For :switch
    #     index [Integer] index of the Pokemon in its party (battle party !, it means the index is not the same as the Pokemon Party index)
    #     team  [Integer] index of the Team (to select the Pokemon from the right trainer)
    def initialize(action_type, target, side, *args)
      @action_type = action_type
      @target = target
      @side = side
      case action_type
      when :attack
        @launcher = args.first
        @skill = args[1]
        @mega_evolve = args.last
      when :item
        @item_id = args.first
        @bag = args.last
      when :switch
        @index = args.first
        @team = args.last
      end
    end
    # Return if the action is a switch
    # @return [Boolean]
    def is_switch?
      return true if @action_type == :switch
      return true if @action_type == :attack and @skill.symbol == :s_u_turn
      return false
    end
    # Return if the action is Focus Punch
    # @return [Boolean]
    def is_focus_punch?
      return (@action_type == :attack and @skill.id == 264)
    end
    # Return a clone for focus punch
    # @param engine [Engine]
    # @return [Action]
    def focus_punch_clone(engine)
      cloned = self.clone
      @mega_evolve = false
      @priority = action_priority(:focus_punch, @launcher, @launcher, @skill, false)
      @mega_priority = @priority - 1
      return clone
    end
    # Calculate the priority of the move by invoking the engine priority calculation, recalculate the launcher if present
    # @param engine [Engine] the engine
    # @param switch [Boolean] if an action in the stack is a switch
    def calc_priority(engine, switch)
      if @launcher
        @launcher = engine.battler(@launcher.position, @launcher.side)
      else
        @launcher = engine.battler(@target, @side)
      end
      @priority = engine.action_priority(@action_type, @launcher, engine.battler(@target, @side), @skill, switch)
      @mega_priority = (@mega_evolve ? @priority : @priority - 1)
    end
    # Execute the action, should be called twice when mega evolve (first => mega evolve, second => attack)
    # @param scene [Scene_Battle] the scene that show the battle
    def execute(scene)
      case @action_type
      when :attack
        if @mega_evolve
          scene.mega_evolve_action(@launcher)
          @mega_evolve = false
        else
          scene.attack_action(@launcher, engine.battler(@target, @side), @skill)
        end
      when :item
        scene.item_use_action(@target, @side, @item_id, @bag)
      when :switch
        scene.switch_action(@target, @side, @index, @team)
      when :flee
        scene.flee_action(@target, @side)
      end
    end
  end
end
