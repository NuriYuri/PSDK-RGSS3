module Battle
  class Engine
    # ID of the Struggle attack
    ID_Struggle = 165
    # ID of the Pursuit attack
    ID_Pursuit = 228
    # Priority delta between each stage
    DeltaPrio = 1_000_000 #Ecart entre deux priorités d'attaque (Normalement au niveau 1000 pas de pb)
    # Base priority of most of battle action
    BattlePrio = 13 * DeltaPrio
    # Priority of the pursuit attack when a switch is done during the trun
    PursuitPrio = 14 * DeltaPrio
    # Priority of the Struggle move
    StrugglePrio = $game_data_skill[ID_Struggle].priority * DeltaPrio
    # Calculate the priority of an action
    # @param type [Symbol] the type of the action
    # @param launcher [PFM::Pokemon_Battler] the launcher of the skill (or the target of the action)
    # @param target [PFM::Pokemon_Battler] the target of the action
    # @param switch [Boolean] if a switch is perfomed during this turn
    def action_priority(type, launcher, target, skill, switch)
      case type
      when :attack, :focus_punch
        spd = launcher.spd
        if type == :focus_punch
          priority = BattlePrio
        elsif switch and skill.id == ID_Pursuit
          priority = PursuitPrio
        else
          priority = skill.priority * DeltaPrio
        end
        # Item checks
        if _has_items(launcher, 316, 279) #>Encens Plein, Ralentiqueue
          priority -= DeltaPrio / 2
        elsif _has_item(launcher, 217) and rand(100) < 20
          # Message ?
          priority += DeltaPrio #>Attaque avant
          spd = -spd #>Mais ne parasite pas
        elsif _has_items(launcher, 215, 278, 289, 290, 291, 292, 293, 294) #>Bracelet Macho, Balle Fer, truc Pouvoir,
          spd /= 2
        elsif launcher.id == 132 and _has_item(launcher, 274) #>Poudre Vite / Métamorph
          spd *= 2
        elsif launcher.item_data.include?(:attack_first)
          priority = BattlePrio
          launcher.item_data.delete(:attack_first)
        end
        # Ability checks
        unless launcher.effect_no_ability?
          if launcher.ability == 60 #> Glissade
            spd *= 2 if $env.rain?
          elsif launcher.ability == 20 #> Chlorophylle
            spd *= 2 if $env.sunny?
          elsif launcher.ability == 145 #> Baigne Sable
            spd *= 2 if $env.sandstorm?
          elsif launcher.ability == 94 #>Frein
            priority -= DeltaPrio / 4 #>Lent mais pas plus que Enscens plein et Ralentiqueue
          end
        end
        #> Vent arrière
        spd *= 2  if @state[launcher.side != 0 ? :enn_tailwind : :act_tailwind] > 0
        #> Le maraicage
        spd /= 2 if @state[launcher.side != 0 ? :enn_swamp : :act_swamp] > 0
        #> Paralysie
        spd /= 2 if pkmn.paralyzed?
        #> Distortion
        spd *= (@state[:trick_room] > 0 ? -1 : 1)
        return priority + spd
      when :flee
        return StrugglePrio
      when :item
        return BattlePrio + 600000
      when :switch
        return BattlePrio + 500000
      end
      return 0 # Fail
    end
    # Generate the action order
    # @param actions [Array<Action>]
    # @return [Array<Action>]
    # @note 
    #   Here I'll separate the BattlePrio actions (including Pursuit, excluding Focus Punch)
    #   collect the mega evolution actions
    #   Sort the BattlePrio actions and mega evolution actions
    #   Add them to the BattlePrio actions
    #   I'll sort the other actions
    #   And then I add the two arrays together
    def make_action_order(actions)
      _calculate_priorities(actions)
      sw_prio = BattlePrio + 500000
      battleprio_actions = actions.select { |action| action.priority >= sw_prio }
      actions -= battleprio_actions
      _merge_focus_punch_action(actions)
      mega_actions = actions.select { |action| action.priority == action.mega_priority }
      #> Sort the actions
      sort_proc = proc { |a, b| b.priority <=> a.priority }
      actions.sort!(&sort_proc)
      mega_actions.sort!(&sort_proc)
      battleprio_actions.sort!(&sort_proc)
      _make_attack_order(actions, battleprio_actions)
      return battleprio_actions + mega_actions + actions
    end
    private
    # Detect if a Switch is present in the action stack
    # @param actions [Array<Action>]
    # @return [Boolean]
    def _switch_in_actions(actions)
      return (actions.each { |action| break true if action.is_switch? } == true)
    end
    # Calculate the priority of each action
    # @param actions [Array<Action>]
    def _calculate_priorities(actions)
      switch = _switch_in_actions(actions)
      actions.each { |action| action.calc_priority(self, switch) }
    end
    # Define the attack order info of the battlers
    # @param actions [Array<Action>]
    # @param battleprio_actions [Array<Action>]
    def _make_attack_order(actions, battleprio_actions)
      #> Reset information about battlers attack
      each_battler do |pokemon|
        pokemon.attack_order = 255
        pokemon.prepared_skill = 0
      end
      #> Set information about battlers attack
      act_ind = 0
      atk_first = nil
      atk_last = 1
      atk_info_proc = proc do |action|
        case action.action_type
        when :attack
          action.launcher.attack_order = act_ind
          act_ind += 1
          atk_first = act_ind unless atk_first
          atk_last = act_ind
          action.launcher.prepared_skill = action.skill.id
        when :switch
          action.target.attack_order = act_ind
          act_ind += 1
        end
      end
      battleprio_actions.each(&atk_info_proc)
      actions.each(&atk_info_proc)
      @attacking_first = atk_first ? atk_first : 0
      @attacking_last = atk_last
    end
    # Collect and add the actions of focus punch
    def _merge_focus_punch_action(actions)
      focus_punch = actions.select { |action| action.is_focus_punch? }
      focus_punch.each do |action|
        actions << action.focus_punch_clone(self)
      end
    end
  end
end
