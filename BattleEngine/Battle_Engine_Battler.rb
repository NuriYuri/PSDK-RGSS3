module Battle
  class Engine
    # Retreive a battler according its side and position
    # @param side [Integer] the side of the battler
    # @param position [Integer] the position of the battler in the side
    # @return [PFM::Pokemon_Battler]
    def battler(position, side)
      pokemon = @battlers[side][position]
      raise "Non battling pokemon retreived" unless pokemon.position
      return pokemon
    end
    # Yield a block on each battlers
    def each_battler
      return unless block_given?
      @battlers.each do |side|
        side.each do |pokemon|
          break unless pokemon.position # The first Pokemon with no position MUST be after every pokemon with position
          yield(pokemon)
        end
      end
    end
    # Yield a block on each ally of the Pokemon
    # @param target [PFM::Pokemon_Battler] the pokemon on that is with the allies
    # @param includes [Boolean] if the target is included in the block yield process or not
    def each_ally(target, includes = false)
      return unless block_given?
      if includes
        @battlers[target.side].each do |pokemon|
          break unless pokemon.position
          yield(pokemon)
        end
      else
        @battlers[target.side].each do |pokemon|
          break unless pokemon.position
          next if pokemon == target
          yield(pokemon)
        end
      end
    end
    # Yield a block on each enemies of the Pokemon
    # @param target [PFM::Pokemon_Battler] the pokemon that is against the enemies
    def each_enemy(target)
      return unless block_given?
      @battlers.each_with_index do |side, index|
        next if index == target.side
        side.each do |pokemon|
          break unless pokemon.position # The first Pokemon with no position MUST be after every pokemon with position
          yield(pokemon)
        end
      end
    end
  end
end
