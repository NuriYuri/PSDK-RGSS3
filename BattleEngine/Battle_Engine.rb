module Battle
  # Engine that does all the calculation and holds all the informations about battle
  class Engine
    # Number of sides (2 : 1v1, 2v2, 3v3, 4: battle royale)
    # @return [2, 4]
    attr_reader :side_count
    # Battlers in the battle (by sides)
    # @return [Array<Array<PFM::Pokemon_Battler>>]
    attr_reader :battlers
    # State of the terrain / battle
    # @param scene [Scene_Battle] the scene that manage the battle
    # @param side_count [Integer] the number of side in the battle
    # @return [Hash]
    attr_reader :state
    def initialize(scene, side_count)
      raise "Invalid number of side" if side_count != 2 and side_count != 4
      @scene = scene
      @battlers = Array.new(side_count) do [] end
      @attacking_first = 0 # Index of the first attacking Pokemon (in the attack order)
      @attacking_last = 0 # Same but with last 
      _state_reset
    end
    # Display a message
    # @param str [String] the message
    def msg(str)
      puts str
      #@scene.display_message(str, true)
    end
  end
end
