module ProjectCompilation
  BOOT_SCRIPT_NAME = 'Scripts.rxdata/Boot (Tu touche ton projet est mort)'
  RELEASE_PATH = 'Release'
  
  @scripts = []
  @next_scripts = []
  
  module_function
  
  def start
    make_release_path
    start_script_compilation
    make_game_rb
  end
  
  def start_script_compilation
    compile_rmxp_scripts
    # Compile script from PSDK
    compile_vscode_scripts(ScriptLoader::VSCODE_SCRIPT_PATH)
    # Compile script from project
    compile_vscode_scripts(ScriptLoader::PROJECT_SCRIPT_PATH)
    save_scripts
  end
  
  def compile(filename, script)
    RubyVM::InstructionSequence.compile(script, File.basename(filename), File.dirname(filename)).to_binary
  end
  
  def compile_rmxp_scripts
    do_next_scripts = false
    load_data('Data/Scripts.rxdata').each do |script_arr|
      script = Zlib::Inflate.inflate(script_arr[2]).force_encoding(Encoding::UTF_8)
      next if script.size < 10 # No short script allowed
      name = "Scripts.rxdata/#{script_arr[1].force_encoding(Encoding::UTF_8)}"
      next(do_next_scripts = true) if name == BOOT_SCRIPT_NAME
      puts "Compiling #{name}"
      (do_next_scripts ? @next_scripts : @scripts) << compile(name, script)
    end
  end

  def compile_vscode_scripts(path)
    compile_scripts(path)
    Dir[File.join(path, '*/')].sort.each { |pathname| compile_scripts(pathname) }
  end

  def compile_scripts(path)
    Dir[File.join(path, '*.rb')].sort.each do |filename|
      next unless File.basename(filename) =~ /^[0-9]{5} .*/
      puts "Compiling #{filename}"
      script = File.read(filename)
      @scripts << compile(filename, script)
    end
  end
  
  def save_scripts
    File.binwrite(File.join(RELEASE_PATH, 'Data', 'Scripts.dat'), Zlib::Deflate.deflate(Marshal.dump(@scripts + @next_scripts)))
    puts "Script saved..."
  end
  
  def make_game_rb
    # Compile real Game.rb
    parse_args = File.read('lib/__parse_argments.rb')
    parse_args.sub!('add(:tags', '# ')
    parse_args.sub!('add(:worldmap', '# ')
    parse_args.sub!('add(:"animation', '# ')
    parse_args.sub!('add(:test', '# ')
    parse_args.sub!('add(:util', '# ')
    game_script = File.read(File.join(PSDK_PATH, '__ReleaseGame.rb'))
    game_script.sub!('{ARGUMENT_PARSE}', parse_args)
    File.binwrite(File.join(RELEASE_PATH, 'Game.yarb'), compile('Game/Boot.rb', game_script))
    # Write Game.rb
    File.write(File.join(RELEASE_PATH, 'Game.rb'), <<-SCRIPT )
RubyVM::InstructionSequence.load_from_binary(File.binread('Game.yarb')).eval
begin
  $GAME_LOOP.call
rescue Exception
  display_game_exception('An error occured during Game Loop.')
end
SCRIPT
  end
  
  def make_release_path
    Dir.mkdir!(File.join(RELEASE_PATH, 'Data', 'PSDK'))
    Dir.mkdir!(File.join(RELEASE_PATH, 'audio', 'bgm'))
    Dir.mkdir!(File.join(RELEASE_PATH, 'audio', 'bgs'))
    Dir.mkdir!(File.join(RELEASE_PATH, 'audio', 'se', 'cries'))
    Dir.mkdir!(File.join(RELEASE_PATH, 'audio', 'me'))
    Dir.mkdir!(File.join(RELEASE_PATH, 'Saves'))
  end
end

rgss_main {}

ProjectCompilation.start