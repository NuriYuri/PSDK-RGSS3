#encoding: utf-8

#> Prevent the game from launching
$GAME_LOOP = proc {}

#> Creation of the output dir
Dir.mkdir!(game_data = "Release/master")
Dir.mkdir!(target_data = "Release/Data")
Dir.mkdir!(psdk_data_path = "Release/Data/PSDK")
Dir.mkdir!(shader_path = "Release/graphics/shaders")

PSDK_MASTER = PSDK_PATH + '/master'
psdk_data = PSDK_PATH + '/Data'

class << File
  # Copy a file to a new path
  # @param filename [String] the filename of the file to copy
  # @param to_path [String] the path where to copy the file
  def copy(filename, to_path)
    target_filename = "#{to_path}/#{File.basename(filename)}"
    return puts("#{target_filename} exists!") if File.exist?(target_filename)
    puts "Copying #{filename}"
    File.open(filename, "rb") do |f|
      File.open(target_filename, "wb") do |copy_f|
        copy_f << f.read(f.size)
      end
    end
  end
end

#> Copy the original PSDK Scripts (/!\ doesn't support plugins yet)
File.copy(psdk_data + "/Scripts.rxdata", target_data)
script_plug = "#{target_data}/Script_Plugin.rxdata"
File.rename("#{target_data}/Scripts.rxdata", script_plug) unless File.exist?(script_plug)
# Copy the boot scripts
File.copy("Data/Scripts.rxdata", target_data)
File.copy("Data/PSDK_BOOT.rxdata", target_data)
# Copy the PSDK Data
Dir['Data/PSDK/*.rxdata'].each do |shader|
  File.copy(shader, psdk_data_path)
end
# Copy the shader
Dir['graphics/shaders/*.txt'].each do |shader|
  File.copy(shader, shader_path)
end
# Copy the archives
Dir[PSDK_MASTER.gsub('\\','/') + "/*"].each do |archive|
  File.copy(archive, game_data)
end
# Prevent Yuki::VD from creating .bak file
class << File
  def rename(*)
  end
end
# Update the archives
[['animation', 'Animations'],
['autotile', 'Autotiles'],
['ball', 'Ball'],
['battleback', 'BattleBacks'],
['battler', 'Battlers'],
['character', 'Characters'],
['fog', 'Fogs'],
['icon', 'Icons'],
['interface', 'Interface'],
['panorama', 'Panoramas'],
['particle', 'Particles'],
['pc', 'PC'],
['picture', 'Pictures'],
['pokedex', 'Pokedex'],
['title', 'Titles'],
['tileset', 'Tilesets'],
['transition', 'Transitions'],
['windowskin', 'Windowskins'],
['foot_print', 'Pokedex/FootPrints'],
['poke_front', 'Pokedex/PokeFront'],
['poke_front_s', 'Pokedex/PokeFrontShiny'],
['poke_back', 'Pokedex/PokeBack'],
['poke_back_s', 'Pokedex/PokeBackShiny'],
['b_icon', 'Pokedex/PokeIcon']].each do |arhive_info|
  filename = "#{game_data}/#{arhive_info.first}"
  path = "Graphics/#{arhive_info.last}".downcase
  puts "Updating #{filename}"
  if Dir.exist?(path)
    vd = Yuki::VD.new(filename, :update)
    Dir.chdir(path) do
      Dir['*.png'].each do |fn|
        vd.add_file(fn.gsub(/\.png/i,'').downcase, 'png')
      end
    end
    vd.close
  end
end
# Making data release
puts "Building data..."
vd = Yuki::VD.new("#{game_data}/data", :write)
data_files = (Dir['Data/*.rxdata'] + Dir['Data/*.dat'] + Dir['Data/*/*.rxdata'] + Dir['Data/*/*.dat'])
data_files -= ['Data/Scripts.rxdata','Data/PSDK_BOOT.rxdata']
data_files.each do |fn|
  vd.add_file(fn.downcase, nil)
end
vd.close
puts "Finished !"