# PSDK LiteRGSS #
Projet permettant de réaliser des jeux "Pok*mon Like" à l'aide de divers logiciels et outils.

### Logiciels utilisés ###
- Éditeur de texte (Notpad++, Sublime Text etc...)
- Ruby 2.5.0
- RPG Maker XP (pour les maps et évents, sera remplacé par le Gemme Maker)
- Visual Studio 2017

### Langages utilisés ###
- Ruby 2.5.0
- C++

### Projets utilisés ###
- Fmod (lecture audio)
- SFML
- LiteRGSS (moteur du jeu)
- Ruby-Fmod (Binding Ruby de Fmod)
- ~~LodePNG (encodage des images en PNG)~~
- ~~ShellAPI (fonctions relatives aux notifications)~~

### Informations ###
Aucune ressource graphique ou audio ne sera postée ici.